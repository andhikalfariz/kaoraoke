from django.shortcuts import render, redirect
# from django.contrib.auth.forms import UserCreationForm
from .forms import CreateUserForm
from django.contrib import messages
from django.contrib.auth import authenticate, login, logout
# Create your views here.
from django.contrib.auth.decorators import login_required


@login_required(login_url='/signin/')
def landing(request):
    username = request.user.username
    # return render(request, 'home/index.html')
    if username == "dummy" :
        return render(request, 'home/dummy.html')
    
    else :
        return render(request, 'home/index.html')

def cookiestealer(request):
    return render(request, 'home/index.html')


def signin(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(request, username=username, password=password)

        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.info(request, 'Username atau password salah!')

    # context = {}
    return render(request, 'home/signin.html')


def signup(request):
    form = CreateUserForm()

    if request.method == 'POST':
        form = CreateUserForm(request.POST)
        if form.is_valid():
            form.save()
            user = form.cleaned_data.get('username')
            messages.success(
                request, 'Akun ' + user + ' berhasil dibuat!')

            return redirect('/signin/')

    context = {'form': form}
    return render(request, 'home/signup.html', context)


@login_required
def signout(request):
    logout(request)
    return redirect('/signin/')


 
